import React, {Component} from 'react';


class Title extends Component {

    render() {

        return (
            <div>
                <h1 className="text-white">
                Weather App React Demo
                </h1>
                <p className="text-white">
                This is a weather call API
                </p>
            </div>
        );
    }
}

export default Title;