import React, {Component} from 'react';


class Weather extends Component {
    render() {
        return (
            <div className="my-5">
                {
                    this.props.country && this.props.city && <p className="text-danger font-weight-bold">
                        Location : <span>
                            {this.props.city} , {this.props.country}
                        </span>
                    </p>
                }

                {
                    this.props.temperature && <p className="text-danger font-weight-bold">
                        Temperature : <span>
                            {this.props.temperature}
                        </span>
                    </p>
                }

                {

                    this.props.humidity && <p className="text-danger font-weight-bold">
                        Humidity : <span>
                            {this.props.humidity}
                        </span>
                    </p>
                }

                {

                    this.props.description && <p className="text-danger font-weight-bold">
                        Description : <span>
                            {this.props.description}
                        </span>
                    </p>
                }

                {
                    this.props.error && <p className="text-danger font-weight-bold">
                        Error : <span>
                            {this.props.error}
                        </span>
                    </p>
                }

            </div>
        );
    }
}



export default Weather;