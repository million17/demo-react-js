import React, {Component, Children} from 'react';

class JsxPractive1 extends Component {

	showInfoProduct(product) {
		if(product.status) {
			return <div className="card mt-3" style={{width: '18rem'}}>
						<img src="https://product.hstatic.net/1000379731/product/iphone-11-pro-select-2019_bcbd5b5e69d64a6da3600a207cfab276_master.jpg" className="card-img-top" alt="..." />
						<div className="card-body">
						<h2>Id : {product.id}</h2>
						<h5 className="card-title">{product.name}</h5>
						<p className="card-text">{product.title}</p>
						<p className="card-text">{product.status ? 'active' : 'no-active'}</p>
						</div>
					</div>
		}
	}
     

    render() {
		var a = 5;
		var c = 3;
		var product = {
			id : 1,
			name : 'I Phone 11 Pro Max',
			image :  'https://product.hstatic.net/1000379731/product/iphone-11-pro-select-2019_bcbd5b5e69d64a6da3600a207cfab276_master.jpg',
			title : 'This is a I Phone 11 Pro Max',
			status : true
		}
		return (
			<div>
				<h2> a ={ a }</h2>
				<h2> c = { c }</h2>
				<h2> a + c ={ a + c}</h2>	
				<div className="card" style={{width: '18rem'}}>
					<img src="https://product.hstatic.net/1000379731/product/iphone-11-pro-select-2019_bcbd5b5e69d64a6da3600a207cfab276_master.jpg" className="card-img-top" alt="..." />
					<div className="card-body">
					<h2>Id : {product.id}</h2>
					<h5 className="card-title">{product.name}</h5>
					<p className="card-text">{product.title}</p>
					<p className="card-text">{product.status ? 'active' : 'no-active'}</p>
					</div>
				</div>
				{this.showInfoProduct(product)}
			</div>
		);
		// <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    }
}


export default JsxPractive1;