import React, {Component} from 'react';
import './App.css';
import Title from './components/weather-app/Title';
import Form from './components/weather-app/Form';
import Weather from './components/weather-app/Weather';

const Api_Key = "8d2de98e089f1c28e1a22fc19a24ef04";
class AppWeather extends Component{

  state = {
    temperature : undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    error: undefined
  }

  getWeather = async (e) => {
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    e.preventDefault();
    try {
      const api_call = await fetch (`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${Api_Key}`);
      const response = await api_call.json();
      console.log(response);
      if(city && country) {
        this.setState({
          temperature: response.main.temp,
          city: response.name,
          country : response.sys.country,
          humidity: response.main.humidity,
          description: response.weather[0].description,
          error: ""
        })
      } else {
        this.setState({
          error : " Please fill all fields ..."
        })
      }
    } catch (error) {
      this.setState( {
        temperature : undefined,
        city: undefined,
        country: undefined,
        humidity: undefined,
        description: undefined,
        error: "Can not find out this city...."
      })
    }
  }

  render() {
    return (
        <div className="container my-5">
            <div className="row">
              <div className="col-sm-5">
                <Title />
              </div>
              <div className="col-sm-7">
                <Form loadWeather={this.getWeather} />
                  <Weather 
                    temperature = {this.state.temperature}
                    city = {this.state.city}
                    country = {this.state.country}
                    humidity = {this.state.humidity}
                    description = {this.state.description}
                    error = {this.state.error}
                  />
              </div>
            </div>
        </div>
      );
  }
}

export default AppWeather;
